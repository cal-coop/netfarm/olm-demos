(in-package :olm-recommender-demo)

#|
For the sake of presentation, we make two compromises that make this different
in implementation to a "real" Netfarm program:

- Objects are stored locally, rather than retrieved from a network. Netfarm is
  network-transparent, of course, so this probably shouldn't be a problem, and
  the program would look roughly the same. We run script effects locally to
  mutate things, so whatever works here would also work on a network.
- Categories are represented in an unrealistic manner; they are collections of
  objects in one slot that cannot be changed. In a real implementation, objects
  would hold a collection of categories, and message each category to add them.
  Then they would be accessible through a computed value. This is similar to the
  implementation of the mutable-mixin, but without any access control.

With that out of the way, here is our example "world":
|#

(defvar *categories* (make-hash-table :test 'equalp))

(clim:define-command
    (com-inspect-category
     :command-table recommender-demo-app
     :name "Inspect Category")
    ((name string))
  (let ((category (gethash name *categories*)))
    (when (null category)
      (format *query-io* "There's no category named ~a.~%" name)
      (return-from com-inspect-category))
    (netfarm-interactor::com-inspect-object category)))

;;; This would be easier to write, would my compiler understand the LIST
;;; instruction.
(netfarm-scripts:define-script *present-category*
    ("name" "header" "articles" "vertically" "text" "is-relevant-to" :false "horizontally")
  (:method "present" 0)
  ;; produce ("header" <name>)
  (get-value 3)
  (get-value 1) self (get-value 0) object-value
  (list 2)
  ;; Get the list of articles.
  self (get-value 2) object-value
  ;; Call the recommender.
  (byte 20)
  (get-value 0)
  ;; ("name" "is-relevant-to" <self>)
  (get-value 0) (get-value 5) self (list 3)
  (get-value 6)
  query-recommender
  ;; Make some list items. 
  (list 0) (get-proc* 0) (call 2)
  ;; ("vertically" <article> ...)
  (get-value 3) swap cons
  (list 3)
  return
  (:procedure 2)
  ;; Make a list item for each article.
  (list 0) (get-env 0 0) equal
  (jump-cond 0 0 0 9)
  ;; Return the reversed accumulator.
  (get-env 1 0) (list 0) (get-proc* 1) (tail-call 2)
  ;; Make the list item, in the form ("horizontally" <title> <article>)
  (get-value 7)
  (get-env 0 0) car dup (get-value 0) object-value swap
  (list 3)
  ;; Cons that on the front of the accumulator.
  (get-env 1 0) cons
  ;; Recurse on the rest of the list.
  (get-env 0 0) cdr swap (get-proc* 0) (tail-call 2)
  (:procedure 2)
  ;; Reverse a list in the first argument, using the second as an accumulator.
  (list 0) (get-env 0 0) equal
  (jump-cond 0 0 0 4)
  ;; Base case: iteration list is empty, return accumulator.
  (get-env 1 0) return
  ;; Bad case: we have more to process.
  ;; Call (reverse (cdr lst) (cons (car lst) acc))
  (get-env 0 0) cdr
  (get-env 0 0) car (get-env 1 0) cons
  (get-proc* 1) (tail-call 2))

(defun truncate-name (name)
  (if (> (length name) 20)
      (concatenate 'string (subseq name 0 20) "…")
      name))

(defclass category ()
  ((name     :initarg :name     :reader name)
   (articles :initarg :articles :reader articles))
  (:scripts *present-category*)
  (:metaclass netfarm:netfarm-class))
(defmethod print-object ((category category) stream)
  (format stream "«Category: ~a»" (truncate-name (name category))))
(defun category (name articles)
  (let ((c (make-instance 'category :name name :articles articles)))
    (setf (gethash name *categories*) c)
    c))

(netfarm-scripts:define-script *present-article*
    ("name" "content" "header" "vertically" "text")
  (:method "present" 0)
  (get-value 3)
  ;; produce ("header" <name>)
  (get-value 2)
  self (get-value 0) object-value
  (list 2)
  ;; produce ("text" <content>)
  (get-value 4)
  self (get-value 1) object-value
  (list 2)
  ;; produce ("vertically" <header> <text>)
  (list 3) return)
  

(defclass article ()
  ((name    :initarg :name    :reader name)
   (content :initarg :content :reader content))
  (:scripts *present-article*)
  (:metaclass netfarm:netfarm-class))
(defmethod print-object ((article article) stream)
  (format stream "«~a»" (truncate-name (name article))))
(defun article (name content)
  (make-instance 'article :name name :content content))

(defvar *articles*
  (list (article "Microsoft announces CLOSOS port in TypeScript"
                 "TSOSOS doesn't slide off the tongue as well.")
        (article "The bailout thread supervision library was bought out by Cyberdyne Systems"
                 "It's like the Terminator. You can blast it in the face with a shotgun--wait, that hasn't happened yet.")
        (article "Can you do my LISP AI homework for me?"
                 "please help me

(defun AddTWOto (
  X
) (
    return-from AddTWOto (+ 3 X
  )
 )
)

this command does not add two to the number")
        (article "C++ adds image dumping support"
                 "Who would use CL in 2020 when C++ gets another release this year and CL hasn't been updated for 25 years??")
        
        (article "Study finds vacuums pose no threat to pets [n=1]"
                 "Abstract: It didn't suck up my horse.")
        (article "Vacuum cleaner sucks up budgie"
                 "Bub-bub-byebye")
        (article "Infamous budgie spared from vacuum filter, rapidly recovering"
                 "Some feathers were lost, but the budgie is glad to not leave the mortal plane in the company of dust bunnies.")
        (article "Help! My rabbit does its business on my control stack, and not in the litter box."
                 "I have a Netlleland dwarf bunny and it refuses to go anywhere other than my control stack.
I tried moving the location of the stack, and it still went there.")))

(defvar *lisp-category*
  (category "Lisp"
            (subseq *articles* 0 4)))
(defvar *pets-category*
  (category "Pets"
            (subseq *articles* 4 8)))

;;; Some other maps to use in collaborative filtering.
(defclass named-user-map (netfarm:user-map)
  ((name :initarg :name :reader map-name))
  (:metaclass netfarm:netfarm-class))
(defmethod print-object ((map named-user-map) stream)
  (format stream "«~a's user-map»" (map-name map)))
(defvar *gnuxie-map*
  (make-instance 'named-user-map
                 :name "Gnuxie"
                 :triples `((,(nth 1 *articles*) "is-relevant-to" ,*lisp-category*)
                            (,(nth 2 *articles*) "is-relevant-to" ,*lisp-category*)
                            (,(nth 5 *articles*) "supersedes" ,(nth 4 *articles*))
                            (,(nth 6 *articles*) "supersedes" ,(nth 5 *articles*)))
                 :antitriples `((,(nth 0 *articles*) "is-relevant-to" ,*lisp-category*)
                                (,(nth 3 *articles*) "is-relevant-to" ,*lisp-category*))))
(defvar *jaidyn-map*
  (make-instance 'named-user-map
                 :name "Jaidyn"
                 :triples `((,(nth 0 *articles*) "is-relevant-to" ,*lisp-category*)
                            (,(nth 1 *articles*) "is-relevant-to" ,*lisp-category*)
                            (,(nth 6 *articles*) "supersedes" ,(nth 5 *articles*)))
                 :antitriples `((,(nth 2 *articles*) "is-relevant-to" ,*lisp-category*))))
(defvar *other-maps* (list *gnuxie-map* *jaidyn-map*))
