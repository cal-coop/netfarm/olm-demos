(in-package :olm-recommender-demo)

(defclass user-recommender (netfarm-scripts:recommender)
  ((user-map :accessor user-map)))

(defclass collaborative-filter-recommender (user-recommender)
  ((search-depth :initarg :search-depth
                 :initform 1
                 :reader search-depth))
  (:documentation "A recommender that uses collaborative filtering to sort objects.
:SEARCH-DEPTH is the number of layers of delegates we'll compute correlations for."))
(defmethod initialize-instance :after
    ((recommender collaborative-filter-recommender) &key)
  (let ((map (make-instance 'netfarm:user-map)))
    (netfarm:add-signature map *keys* *user*)
    (setf (slot-value recommender 'user-map)
          map)))
        

(defun map-facts (user-map)
  (union (netfarm:user-map-triples user-map)
         (netfarm:user-map-antitriples user-map)
         :test #'netfarm-scripts::netfarm-equal))
(defun map-value (user-map fact)
  (cond
    ((member fact (netfarm:user-map-triples user-map)
             :test #'netfarm-scripts::netfarm-equal)
     1.0)
    ((member fact (netfarm:user-map-antitriples user-map)
             :test #'netfarm-scripts::netfarm-equal)
     -1.0)
    (t 0.0)))

(defun correlation (map1 map2)
  (let ((facts (intersection (map-facts map1)
                             (map-facts map2)
                             :test #'netfarm-scripts::netfarm-equal)))
    (when (null facts)
      (return-from correlation 0.0))
    (loop for fact in facts
          summing (* (map-value map1 fact)
                     (map-value map2 fact))
            into sum
          finally (return (/ sum (length facts))))))

(defun latest-version (user-map)
  (loop
    (let ((next-maps (netfarm:object-successors user-map)))
      (when (null next-maps)
        (return-from latest-version user-map))
      (setf user-map (first next-maps)))))

(defgeneric compute-correlations (recommender)
  (:method ((recommender collaborative-filter-recommender))
    (let ((table (make-hash-table))
          (our-map (user-map recommender)))
      ;; The user's user-map will always have perfect correlation with itself.
      (setf (gethash our-map table) 1.0)
      (labels ((recurse (user-map depth)
                 (when (minusp depth)
                   (return-from recurse))
                 (setf user-map (latest-version user-map))
                 (when (null (gethash user-map table))
                   (setf (gethash user-map table)
                         (correlation our-map user-map)))
                 (recurse-into-delegates user-map depth))
               (recurse-into-delegates (user-map depth)
                 (dolist (delegate (netfarm:user-map-delegates user-map))
                   (recurse delegate (1- depth)))))
        (recurse-into-delegates our-map (search-depth recommender)))
      table)))
    
(defgeneric pr (recommender query correlations)
  (:method ((recommender collaborative-filter-recommender)
            query correlations)
    (let ((value-sum 0.0)
          ;; Setting the count to a non-zero number reduces the magnitude of a
          ;; prediction with few samples to go by. It also avoids division by
          ;; zero if there are no samples. 
          (count 0.5))
      (maphash (lambda (map value)
                 (incf value-sum (* value (map-value map query)))
                 (incf count (abs value)))
               correlations)
      (/ value-sum count))))

(defmethod netfarm-scripts:sort-object-list
    ((recommender collaborative-filter-recommender) objects query end)
  (let ((correlations (compute-correlations recommender)))
    (sort (copy-list objects) #'>
          :key (lambda (object)
                 (pr recommender
                     (netfarm-scripts:substitute-object-into-query object query)
                     correlations)))))
