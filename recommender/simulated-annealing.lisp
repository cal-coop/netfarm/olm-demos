(defpackage :simulated-annealing
  (:use :cl)
  (:export #:simulated-annealing
           #:sequence-state
           #:sequence-state->vector
           #:sequence-state-neighbour
           #:sequence-state-position
           #:sequence-state-ref
           #:sequence-state-map))
(in-package :simulated-annealing)

(defun acceptance (old-e new-e temperature)
  (if (< new-e old-e)
      1.0
      (exp (- (/ (- new-e old-e) temperature)))))
(defun accept? (old-e new-e temperature)
  (> (acceptance old-e new-e temperature)
     (random 1.0)))
(defun gaussian-offset (position)
  (+ position
     (round (* (alexandria:gaussian-random) 3))))

(defun simulated-annealing (initial-state cost-function neighbour-function
                            &key (steps 100)
                                 (initial-temperature 1.0)
                                 (temperature-multiplier 0.99)
                                 (minimum-cost 0.0))
  (let* ((temperature initial-temperature)
         (state initial-state)
         (cost (funcall cost-function state)))
    (dotimes (step steps)
      (let* ((new-state (funcall neighbour-function state))
             (new-cost  (funcall cost-function new-state)))
        (when (accept? cost new-cost temperature)
          (setf state  new-state
                cost   new-cost)
          (when (<= cost minimum-cost 0.0)
            (return)))
        (setf temperature (* temperature temperature-multiplier))))
    (values state cost)))

;;; A "sequence" that allows for efficient copying and POSITION taking.
(defstruct sequence-state
  (forward nil :read-only t)
  (backward nil :read-only t)
  (length 0 :read-only t))

(defmethod print-object ((state sequence-state) stream)
  (print-unreadable-object (state stream :type t)
    (write-string "#(" stream)
    (dotimes (n (if (null *print-length*)
                    (sequence-state-length state)
                    (min (sequence-state-length state)
                         *print-length*)))
      (write (hamt:dict-lookup (sequence-state-forward state)
                               n)
             :stream stream)
      (write-char #\Space stream))
    (write-string ")" stream)))

(defun sequence-state (sequence)
  (let ((forwards-hamt (hamt:empty-dict :test #'= :hash #'identity))
        (backward-hamt (hamt:empty-dict :test #'equal))
        (n 0))
    (map 'nil
         (lambda (element)
           (setf forwards-hamt (hamt:dict-insert forwards-hamt n element))
           (setf backward-hamt (hamt:dict-insert backward-hamt element n))
           (incf n))
         sequence)
    (make-sequence-state :forward forwards-hamt
                         :backward backward-hamt
                         :length (length sequence))))
(defun sequence-state->vector (sequence-state)
  (let* ((length (sequence-state-length sequence-state))
         (vector (make-array length))
         (forwards-hamt (sequence-state-forward sequence-state)))
    (dotimes (n length)
      (setf (aref vector n)
            (cl-hamt:dict-lookup forwards-hamt n)))
    vector))

(defun sequence-state-swap (sequence-state position-1 position-2)
  (let* ((forwards-hamt (sequence-state-forward sequence-state))
         (backward-hamt (sequence-state-backward sequence-state))
         (element-1 (cl-hamt:dict-lookup forwards-hamt position-1))
         (element-2 (cl-hamt:dict-lookup forwards-hamt position-2)))
    (make-sequence-state :forward (hamt:dict-insert forwards-hamt
                                                    position-1 element-2
                                                    position-2 element-1)
                         :backward (hamt:dict-insert backward-hamt
                                                     element-1 position-2
                                                     element-2 position-1)
                         :length (sequence-state-length sequence-state))))

(defun sequence-state-neighbour (sequence-state)
  "Swap two elements in SEQUENCE-STATE"
  (let ((length (sequence-state-length sequence-state)))
    (assert (>= length 2))
    (loop for position-1 = (random length)
          for position-2 = (gaussian-offset position-1)
          when (and (< 0 position-2) (< position-2 length)
                    (/= position-1 position-2))
            return (sequence-state-swap sequence-state
                                        position-1 position-2))))

(defun sequence-state-position (sequence-state element)
  (cl-hamt:dict-lookup (sequence-state-backward sequence-state) element))
(defun sequence-state-ref (sequence-state element)
  (cl-hamt:dict-lookup (sequence-state-forward sequence-state) element))
(defun sequence-state-map (function sequence-state)
  (hamt:dict-reduce (lambda (bogus key value)
                      (declare (ignore bogus))
                      (funcall function key value))
                    (sequence-state-forward sequence-state)
                    '#:bogus))
