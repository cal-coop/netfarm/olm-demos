(in-package :olm-recommender-demo)

;;; An "identity" recommender.
(defclass identity-recommender (netfarm-scripts:recommender)
  ())

(defmethod netfarm-scripts:sort-object-list
    ((recommender identity-recommender) objects query end)
  "An identity-recommender returns objects in the order they were provided."  
  (declare (ignore query end))
  (subseq objects 0 end))

(defvar *recommender* (make-instance 'collaborative-filter-recommender))

(defun reset-recommender ()
  (setf *recommender*
        (make-instance (class-of *recommender*))))

(defun run-demo ()
  (netfarm-scripts:with-recommender (*recommender*)
    (clim:run-frame-top-level
     (clim:make-application-frame 'recommender-demo-app))))
