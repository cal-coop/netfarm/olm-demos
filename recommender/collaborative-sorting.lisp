(in-package :olm-recommender-demo)

(defclass collaborative-sorting-recommender (collaborative-filter-recommender)
  ())

(defmethod cl-murmurhash:murmurhash ((object netfarm:object) &key seed mix-only)
  (cl-murmurhash:murmurhash (netfarm:hash-object* object)
                            :seed seed :mix-only mix-only))

(defun user-map-relations (user-map verb)
  "Produce a hash table, mapping each subject to every object that they relates to by triples with the given verb."
  (let ((table (make-hash-table :test 'eql)))
    (labels ((add-relations-from-map (map)
               (add-relations-from-list
                (netfarm:user-map-triples map))
               (add-relations-from-list
                (netfarm:user-map-antitriples map))
               (mapc #'add-relations-from-map
                     (netfarm:user-map-delegates map)))
             (add-relations-from-list (list)
               (dolist (triple list)
                 (destructuring-bind (subject verb* object) triple
                   (when (equal verb verb*)
                     (pushnew object (gethash subject table)
                              :test #'equal))))))
      (add-relations-from-map user-map)
      table)))

(defun sort-by-supersedes-relationships (recommender list)
  "Sort a list so that superseding articles appear before the articles they supersede."
  (when (< (length list) 2)
    (return-from sort-by-supersedes-relationships list))
  (let* ((user-map (latest-version (user-map recommender)))
         (relations (user-map-relations user-map "supersedes"))
         (initial-state (simulated-annealing:sequence-state list))
         (correlations (compute-correlations recommender)))
    (flet ((cost (state)
             (let ((total-cost 0.0))
               (simulated-annealing:sequence-state-map
                (lambda (position value)
                  (dolist (superseded (gethash value relations))
                    ;; Increment the cost by the probability that an object
                    ;; should be superseded by an object appearing after it.
                    (let ((probability (pr recommender `(,value "supersedes" ,superseded)
                                           correlations)))
                      (when (and (plusp probability)
                                 (< (simulated-annealing:sequence-state-position state superseded)
                                    position))
                        (incf total-cost probability)))))
                state)
               total-cost)))
      ;; Try to sort the list, such that as many supersedes relationships are
      ;; reflected correctly in the ordering as possible.
      (coerce (simulated-annealing:sequence-state->vector
               (simulated-annealing:simulated-annealing
                initial-state
                #'cost
                #'simulated-annealing:sequence-state-neighbour
                :initial-temperature 10.0
                :steps 20
                :temperature-multiplier 0.99))
              'list))))

(defmethod netfarm-scripts:sort-object-list
    ((recommender collaborative-sorting-recommender) objects query end)
  (sort-by-supersedes-relationships recommender (call-next-method)))
    
