(asdf:defsystem :olm-recommender-demo
  :depends-on (:cl-hamt :netfarm-interactor :clim :alexandria :clouseau)
  :serial t
  :components ((:file "simulated-annealing")
               (:file "package")
               (:file "interactor-class")
               (:file "example-world")
               (:file "collaborative-filtering")
               (:file "collaborative-sorting")
               (:file "facts-interface")
               (:file "recommender")))
