(in-package :olm-recommender-demo)

(defvar *keys* (netfarm:generate-keys))
(defvar *user* (netfarm:keys->object *keys*))

(defclass recommender-demo-app
    (netfarm-interactor:netfarm-interactor-app)
  ()
  (:default-initargs
   :command-table (clim:find-command-table 'recommender-demo-app)))

(clim:define-command-table recommender-demo-app
    :inherit-from (netfarm-interactor:netfarm-interactor-app))

(clim:define-command (com-inspect-in-lisp
                      :name "Inspect In Lisp"
                      :command-table recommender-demo-app)
    ()
  (clouseau:inspect
   (netfarm-interactor:interactor-object clim:*application-frame*)
   :new-process t))
